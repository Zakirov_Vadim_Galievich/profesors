-- auto-generated definition
CREATE TABLE programm
(
    id     INTEGER PRIMARY KEY,
    name   VARCHAR(64) not null,
    course VARCHAR(64)
);

CREATE TABLE professor_and_programms
(
    id INTEGER PRIMARY KEY,
    prog_id INTEGER REFERENCES programm(id),
    prof_id INTEGER REFERENCES proffessors(id)

);

INSERT INTO proffessors (id, firstname, lastname, salary, academic_hours_in_month) VALUES ('9', 'Вадим','Закиров','50000','5');

SELECT firstname, lastname FROM proffessors WHERE lastname = 'Закиров' AND firstname = 'Вадим';

ALTER TABLE programm DROP id;

SELECT id, firstname, lastname, salary, academic_hours_in_month, snils FROM proffessors WHERE snils = 1234567780;

SELECT MAX(id) FROM proffessors;

INSERT INTO professor_and_programms (id, prog_id, prof_id) VALUES (2, 8, 4);

SELECT * FROM programm WHERE NOT EXISTS (SELECT * FROM proffessors WHERE proffessors.prog_id = programm.id);

SELECT COUNT(*) FROM proffessors;

ALTER TABLE programm ADD COLUMN prof_id BIGSERIAL PRIMARY KEY;
ALTER TABLE professor_and_programms ADD COLUMN prof_id INTEGER REFERENCES proffessors(id);

SELECT COUNT(*) AS exact_count FROM proffessors WHERE snils = 1300504796;

DELETE FROM proffessors WHERE snils = 1234567890






