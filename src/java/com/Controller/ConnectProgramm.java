package com.Controller;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectProgramm {

    private static final String JDBCURL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "3658";


    public static Connection connectToServer() {
        try {
            Connection connection = DriverManager.getConnection(JDBCURL, USERNAME, PASSWORD);
            //System.out.println("Connected to PostgreSQL server in ConnectedClass");
            return connection;
        } catch (SQLException ex) {
            System.out.println("Error in connecting yo PostgreSQL server in ConnectedClass");
            ex.printStackTrace();
        }
        return null;
    }
}

