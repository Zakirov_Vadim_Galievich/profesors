package com.Controller;

import com.Model.Course;
import com.Model.Create;
import com.Model.Search;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static com.Model.Delete.deletePerson;
import static com.Model.Search.search;

public class Control implements Runnable {
    private int mode;

    public Control(int mode) {
        this.mode = mode;
    }

    public void runToCreate() {
        HashMap<Integer, Course> mapProgram = Search.searchigVacancy();
        if (!mapProgram.isEmpty()) {
            Scanner in = new Scanner(System.in);
            System.out.println("Please set a professor's firstname whose you want to set");
            String firstname = in.nextLine();

            System.out.println("Please set a professor's lastname whose you want to set");
            String lastname = in.nextLine();

            System.out.println("Please set a professor's hours whose you want to set");
            int hours = in.nextInt();

            System.out.println("Please set a professor's baserate whose you want to set");
            double baserate = in.nextDouble();

            System.out.println("Please set a professor's snils whose you want to set");
            long snils = in.nextLong();

            System.out.println("Please choose the program in which the professor will be work");


            for (Map.Entry<Integer, Course> e : mapProgram.entrySet()) {
                System.out.format("Choose and enter the id of program for %s %s:", firstname, lastname);
                System.out.println();
                System.out.format("id: %d, name: %s.", e.getKey(), mapProgram.get(e.getKey()).getName());
                System.out.println();
            }
            int idOfProgram = in.nextInt();

            Create create = new Create();
            create.setProf(firstname, lastname, hours, baserate, snils, idOfProgram);
            in.close();

        } else {
            System.out.println("All vacancies are taken, please vacate a vacancy.");
        }
    }

    public void runToSearch() {
        Scanner in = new Scanner(System.in);
        System.out.println("Do you want to find a professor or course. P - professor, C - course");
        String decide = in.nextLine();
        if (decide.equals("P")) {
            System.out.println("Please set the professor's snils (СНИЛС)");
            long snils = in.nextLong();
            in.close();
            System.out.println(search(snils).toString());
        } else if (decide.equals("C")) {
            System.out.println("Please write the course's id");
            int idCourse = in.nextInt();
            in.close();
            System.out.println(search(idCourse).toString());
        } else {
            System.out.println("You wrote incorrect symbol");
        }
    }

    public void runToDelete() {
        System.out.println("Please set the professor's SNILS");

        Scanner in = new Scanner(System.in);
        long snils = in.nextLong();
        in.close();
        deletePerson(snils);
        System.out.println("The professor was deleted");
    }

    @Override
    public void run() {
        if (mode == 1) {
            runToCreate();
        }
        if (mode == 2) {
            runToSearch();
        }
        if (mode == 3) {
            runToDelete();
        }
    }
}
