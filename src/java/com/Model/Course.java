package com.Model;

public class Course {
    private int id;
    private String name;
    private String course;
    private int identifire;

    @Override
    public String toString() {
        return "Course{" +
                "id = " + id +
                ", name = '" + name + '\'' +
                ", course = '" + course + '\'' +
                ", identifire = " + identifire +
                '}';
    }

    public Course(int id, String name, String course, int identifire) {
        this.id = id;
        this.name = name;
        this.course = course;
        this.identifire = identifire;
    }

    public String getName() {
        return name;
    }

    public String getCourse() {
        return course;
    }

    public int getIdentifire() {
        return identifire;
    }

    public Course(String name, String course, int identifire) {
        this.name = name;
        this.course = course;
        this.identifire = identifire;
    }

    public int getId() {
        return id;
    }
}
