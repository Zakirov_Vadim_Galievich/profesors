package com.Model;

import java.sql.*;
import java.util.*;

import static com.Controller.ConnectProgramm.connectToServer;
import static com.Model.Search.*;

public class Create {

    private HashMap<Integer, Course> mapVacancies;

    public Create() {
        this.mapVacancies = searchigVacancy();
    }

    public boolean setProf(String firstname, String lastname, int hours, double baserate, long snils, int idOfProgram) {
        Connection connection = connectToServer();

        double salary = createSalary(hours, baserate, snils);

        Person person = new Person(firstname, lastname, hours, salary, snils);
        try {
            //Add professor to the proffessors table.
            PreparedStatement statement =
                    connection.prepareStatement("INSERT INTO proffessors (firstname, lastname, salary, academic_hours_in_month, snils, prog_id)"
                            + "VALUES (?,?,?,?,?,?)");
            statement.setString(1, firstname);
            statement.setString(2, lastname);
            statement.setDouble(3, salary);
            statement.setInt(4, hours);
            statement.setLong(5, snils);
            statement.setInt(6, idOfProgram);


            int id = getId(snils);
            person.setId(id);

            int rows = statement.executeUpdate();
            if (rows > 0) {
                System.out.println("The contact " + firstname + " " + lastname + " has been inserted.");
            }

            connection.close();
            statement.close();

            return true;
        } catch (SQLException e) {
            System.err.println("Error in connecting to PostgreSQL server");
            e.printStackTrace();
        }
        return false;
    }

    /*
    public boolean createCourse(String name, String course, int identifier) {
        Connection connection = connectToServer();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO programm (id, name, course, identifier)"
                    + "VALUES (?, ?, ?, ?)");
            preparedStatement.setInt(1, );
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, course);
            preparedStatement.setInt(4, identifier);


        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }*/

    public double createSalary(int hours, double baseRate, long snils) {
        double cof = 1;
        int count = 1;

        Connection connection = connectToServer();
        try (PreparedStatement preparedStatement = connection.prepareStatement("SELECT COUNT(*) AS exact_count FROM proffessors WHERE snils = ?")) {
            preparedStatement.setLong(1, snils);
            try (ResultSet rs = preparedStatement.executeQuery()) {
                while (rs.next()) {
                    count = rs.getInt("exact_count");
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        if (count == 0) {
            count = 1;
        }
        cof = cof * count;
        double part1 = ((hours * 3 * baseRate) / 4) * cof;
        return part1;
    }

}
