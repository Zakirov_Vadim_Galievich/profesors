package com.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static com.Controller.ConnectProgramm.connectToServer;

public class Delete {
    public static boolean deletePerson(long snils) {
        int rowsAffected = 0;
        try (Connection connection = connectToServer(); PreparedStatement preparedStatement =
                connection.prepareStatement("DELETE FROM proffessors WHERE snils = ?");) {
            preparedStatement.setLong(1, snils);
            rowsAffected = preparedStatement.executeUpdate();
            if (rowsAffected > 0) {
                return true;
            }
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return false;
    }
}
