package com.Model;

public class Person {
    private String firstname;
    private String lastname;
    private int hours;
    private int baserate;
    private double salary;
    private int id;
    private long snils;

    public int setId(int id) {
        int ident = Search.getId(getSnils());
        if(id == ident) {
            this.id = id;
            return 1;
        } else {
            return 0;
        }
    }

    public Person(String firstname, String lastname, int hours, double salary, long snils) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.hours = hours;
        this.salary = salary;
        this.id = id;
        this.snils = snils;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public int getHours() {
        return hours;
    }
    public long getSnils() {
        return snils;
    }

    public double getSalary() {
        return salary;
    }

    public double getBaserate() {
        return baserate;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstname = '" + firstname + '\'' +
                ", lastname = '" + lastname + '\'' +
                ", hours = " + hours +
                ", baserate = " + baserate +
                ", salary = " + salary +
                ", id = " + id +
                ", snils = " + snils +
                '}';
    }
}
