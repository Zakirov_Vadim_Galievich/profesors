package com.Model;

import java.util.HashMap;

public class Persons {
    private static HashMap<Integer, Person> persons = new HashMap<>();

    public static boolean savePerson(Person person) {
        if(!persons.containsKey(person.getId())) {
            persons.put(person.getId(), person);
            return true;
        }else {
            System.out.println("Person with id " + person.getId() + " has been existed");
            return false;
        }
    }

    public static Person getPerson(int id) {
        if(persons.containsKey(id)) {
            return persons.get(id);
        } else {
            System.out.println("Person with id " + id + " not found");
            return null;
        }
    }
}
