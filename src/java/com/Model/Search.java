package com.Model;

import java.sql.*;
import java.util.*;

import static com.Controller.ConnectProgramm.connectToServer;

public class Search {

    //Looking for a person by lastname
    public static Person search(long snils) {

        int id = 0;
        String lastname = null;
        String firstname = null;
        int hours = 0;
        double salary = 0.0;
        long snilsPerson = 0;

        Connection connection = connectToServer();
        try (Statement statement = connection.createStatement(); PreparedStatement resultSet = connection.prepareStatement("SELECT id, firstname, lastname, salary, academic_hours_in_month, snils FROM proffessors WHERE snils = ?")) {
            resultSet.setLong(1, snils);
            try (ResultSet rs = resultSet.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("id");
                    firstname = rs.getString("firstname");
                    lastname = rs.getString("lastname");
                    salary = rs.getDouble("salary");
                    hours = rs.getInt("academic_hours_in_month");
                    snilsPerson = rs.getLong("snils");

                    Person foundPerson = new Person(firstname, lastname, hours, salary, snilsPerson);
                    resultSet.close();
                    statement.close();
                    connection.close();
                    return foundPerson;
                }
            }
            return null;
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    // Looking for a coursename by identifier of course.
    public static Course search(int identifier) {
        int id = 0;
        String name = null;
        String course = null;
        int ident = 0;

        Connection connection = connectToServer();
        try (Statement statement = connection.createStatement(); PreparedStatement resultSet = connection.prepareStatement("SELECT * FROM programm WHERE identifier = ?")) {
            resultSet.setInt(1, identifier);
            try (ResultSet rs = resultSet.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("id");
                    name = rs.getString("name");
                    course = rs.getString("course");
                    ident = rs.getInt("identifier");

                    Course foundCourse = new Course(id, name, course, ident);
                    resultSet.close();
                    statement.close();
                    connection.close();
                    return foundCourse;
                }
            }
            resultSet.close();
            statement.close();
            connection.close();
            return null;
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return null;
        }
    }

    public static HashMap<Integer, Course> searchigVacancy() {
        HashMap<Integer, Course> vacancies = new HashMap<>();

        Connection connection = connectToServer();
        try (Statement statement = connection.createStatement();
             PreparedStatement resultSet = connection.prepareStatement("SELECT * FROM programm WHERE NOT EXISTS (SELECT * FROM proffessors WHERE proffessors.prog_id = programm.id)")) {

            int id = 0;
            String name = null;
            String course = null;
            int ident = 0;

            try (ResultSet rs = resultSet.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("id");
                    name = rs.getString("name");
                    course = rs.getString("course");
                    ident = rs.getInt("identifier");

                    Course foundCourse = new Course(id, name, course, ident);
                    vacancies.put(id, foundCourse);
                }
            }
            return vacancies;
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            return vacancies;
        }
    }

    public static int getId(long snils) {
        int id = 0;
        Connection connection = connectToServer();
        try (Statement statement = connection.createStatement();
             PreparedStatement resultSet = connection.prepareStatement("SELECT id FROM proffessors WHERE snils = " + snils)) {

            try (ResultSet rs = resultSet.executeQuery()) {
                while (rs.next()) {
                    id = rs.getInt("id");
                }
            }
        } catch (SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("id had not been gotten");
        }
        return id;
    }
}
