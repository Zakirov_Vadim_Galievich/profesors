package com.View;

import com.Controller.Control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ViewP {

    public static void setMode() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            System.out.println("Please choose one of the mode of program: create person: \"c\", search the person: \"s\", delete a person: \"d\"");
            String mode = reader.readLine();
            switch (mode) {
                case "c":
                    Control control = new Control(1);
                    Thread thread = new Thread(control);
                    thread.start();
                    thread.join();
                    break;
                case "s":
                    Control control1 = new Control(2);
                    Thread thread1 = new Thread(control1);
                    thread1.start();
                    thread1.join();
                    break;
                case "d":
                    Control control2 = new Control(3);
                    Thread thread2 = new Thread(control2);
                    thread2.start();
                    thread2.join();
                    break;
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }
}
